//
//  NewBooksTableViewCell.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

protocol FavoriteAppCollectionViewCellDelegate: class {
    
   // func navigateToWebApplicationFromFavSection(webApplication: Application);
    
   // func navigateToAppDetail(application: Application)
    
}

class NewBooksTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {
    
        weak var delegate:FavoriteAppCollectionViewCellDelegate?

        @IBOutlet weak var newBooksCollectionViewCell: UICollectionView!
    
    //        //TEMP BANNER URLS
    var bannerArray: [String] = ["https://s3-eu-west-1.amazonaws.com/developer-application-test/images/11.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/12.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/8.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/5.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/4.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg"
        ,"https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg",
         "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/8.jpg"
    ]
    //
    //        //TheBestHunderedBooksCell
    var bestCollectionViewCell: BestCollectionViewCell?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.initCollectionViewAndRegisterCell()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
    func initCollectionViewAndRegisterCell() {
        
        //set delegate and datasource
        self.newBooksCollectionViewCell.delegate = self;
        self.newBooksCollectionViewCell.dataSource = self;
        
        //register cell
        self.newBooksCollectionViewCell.register(UINib(nibName: "NewBooksCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "NewBooksCollectionViewCell")
        
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return bannerArray.count;
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let newBooksCollectionViewCell = self.newBooksCollectionViewCell.dequeueReusableCell(withReuseIdentifier: "NewBooksCollectionViewCell", for: indexPath) as? NewBooksCollectionViewCell
        
        if let url = URL(string: bannerArray[indexPath.row]) {
            
            DispatchQueue.global().async {
                
                DispatchQueue.main.async {
                    
                    newBooksCollectionViewCell?.favoriteAppImageView.kf.indicatorType = .activity;
                    
                    newBooksCollectionViewCell?.favoriteAppImageView.kf.setImage(with: url)
                    
                }
            }
            
        }
        
        
        return newBooksCollectionViewCell!
        
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        if indexPath.row == 0 {
            
        }
        
    }
    
        
}

