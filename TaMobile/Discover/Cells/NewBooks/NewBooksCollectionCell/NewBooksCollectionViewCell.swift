//
//  NewBooksCollectionViewCell.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

class NewBooksCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var favoriteAppImageView: UIImageView!
    
    @IBOutlet weak var favoriteAppNameTextView: UILabel!
    
    @IBOutlet weak var imageLoadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.layer.cornerRadius = 10;
        self.layer.masksToBounds = true;
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 4.0)
        self.layer.shadowRadius = 10
        self.layer.shadowOpacity = 0.6
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
        self.favoriteAppImageView.layer.cornerRadius = 10;
        self.favoriteAppImageView.layer.masksToBounds = true;
        
    }
    
    
    
}
