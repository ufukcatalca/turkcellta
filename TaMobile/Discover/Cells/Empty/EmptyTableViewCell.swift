//
//  EmptyTableViewCell.swift
//
//  Created by Horizon on 4.12.2017.
//  Copyright © 2017. All rights reserved.
//

import UIKit

class EmptyTableViewCell: UITableViewCell {

    
    @IBOutlet weak var emptyImageView: UIImageView!

    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
