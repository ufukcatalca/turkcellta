//
//  AppDetailViewController.swift
//  
//
import UIKit
import Kingfisher
import NVActivityIndicatorView

class AppDetailViewController: UIViewController, UITextViewDelegate{
    
    //DataManager
    //var dataManager:DataManager = DataManager.sharedDataManager;
    
    //searchedAppId
    var searchedAppId:String?
    
    
    
    //activity
    
    var activity: NVActivityIndicatorView?
    
    //app status
    var appFavoriteStatus:Bool?
    var appDownloadStatus:Bool?
    
    //outlets
    
    @IBOutlet weak var appCoverImageView: UIImageView!
    
    @IBOutlet weak var appIconImageView: UIImageView!
    
    @IBOutlet weak var appNameLabel: UILabel!
    
    @IBOutlet weak var appPlatformImageView: UIImageView!
    
    @IBOutlet weak var appVersionLabel: UILabel!
    
    @IBOutlet weak var appCategoryLabel: UILabel!
    
    @IBOutlet weak var appFavoriteButton: UIButton!
    
    @IBOutlet weak var appOpenDownloadButton: UIButton!
    
    @IBOutlet weak var appDescriptionTextView: UITextView!
    
    @IBOutlet weak var appDescriptionView: UIView!
    //actions
    
    
    
    
    @IBAction func appFavoriteButtonAction(_ sender: Any) {
        
    }
    
    @IBAction func appOpenDownloadButtonAction(_ sender: Any) {
        
//        if Application?.platformType == "webapps" {
//
//            self.performSegue(withIdentifier: "webAppSegueFromDetail", sender: self);
//
//        }
//        else {
//
//            if self.appDownloadStatus == false {
//
//                MKUtilityManager.openAppWithURLSchema(urlSchema: (self.Application?.bundleId)!, userId: MKUser.sharedMKUser.id!, userPassword: MKUser.sharedMKUser.password!)
//
//            }
//            else {
//
//                if let appPathUrl = self.Application?.appFilePath {
//
//                    MKDownloadManager.downloadIpaFromPlistUrl(plistUrl: appPathUrl)
//
//                }
//
//            }
//
//        }
        
    }
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        //self.setBackButtonAppearance()
        
        if let appId = self.searchedAppId {
            
            //self.Application = dataManager.getAppWithAppId(appId: appId)
            
        }
        
        self.setAppInfoDetail()
        
        self.controlApplicationDownloadStatus()
        
        self.controlApplicationFavoriteStatus()
        
        // Do any additional setup after loading the view.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.setTextViewCornerRadiusAndShadow()
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.setBackButtonAppearance()
        
    }
    
    func textViewShouldBeginEditing(_ textView: UITextView) -> Bool {
        
        self.appDescriptionTextView.contentInset = UIEdgeInsets.zero
        
        return true;
        
    }
    
    func setAppInfoDetail() {
        
//        if let url = URL(string: (Application?.appIconUrl!)!) {
//
//            DispatchQueue.global().async {
//
//                DispatchQueue.main.async {
//
//                    self.appIconImageView.kf.indicatorType = .activity
//                    self.appIconImageView.kf.setImage(with: url)
//                    self.appIconImageView.layer.cornerRadius = 10
//                    self.appIconImageView.layer.masksToBounds = true;
//
//                }
//
//            }
//
//        }
//
//        if let coverUrl = URL(string: (Application?.appCoverPhotoUrl!)!) {
//
//            DispatchQueue.global().async {
//
//                DispatchQueue.main.async {
//
//                    self.appCoverImageView.kf.indicatorType = .activity
//                    self.appCoverImageView.kf.setImage(with: coverUrl)
//
//                }
//
//            }
//
//        }
//
//        self.appNameLabel.text = Application?.name
//        self.appVersionLabel.text = Application?.version
//        self.appCategoryLabel.text = Application?.category
//        self.appDescriptionTextView.text = Application?.appDescription
//
//        var platformImage:UIImage?
//
//        if Application?.platformType == "webapps" {
//
//            platformImage = UIImage(named: "browser")
//
//        }
//        else {
//
//            platformImage = UIImage(named: "cellPhone")
//
//        }
//
//        self.appPlatformImageView.image = platformImage
        
    }
    
    func setTextViewCornerRadiusAndShadow() {
        
        self.appDescriptionView.layer.cornerRadius = 10;
        self.appDescriptionView.layer.masksToBounds = true;
        self.appDescriptionView.layer.shadowColor = UIColor.lightGray.cgColor
        self.appDescriptionView.layer.shadowOffset = CGSize(width: 0, height: 2.0)
        self.appDescriptionView.layer.shadowRadius = 10
        self.appDescriptionView.layer.shadowOpacity = 0.6
        self.appDescriptionView.layer.masksToBounds = false;
        self.appDescriptionView.layer.shadowPath = UIBezierPath(roundedRect: self.appDescriptionView.bounds, cornerRadius: self.appDescriptionView.layer.cornerRadius).cgPath
        
    }
    
    func controlApplicationFavoriteStatus() {
        
//        if self.Application?.isFavorite == true {
//
//            //favorited
//            self.appFavoriteButton.setBackgroundImage(UIImage(named: "favorited"), for: .normal);
//            self.appFavoriteButton.setImage(UIImage(named: "orange-heart"), for: .normal);
//            self.appFavoriteButton.setTitle(" FAVORİ", for: .normal);
//            self.appFavoriteButton.setTitleColor(UIColor(red: 254/255, green: 174/255, blue: 0, alpha: 1.0), for: .normal);
//            self.appFavoriteButton.tintColor = UIColor(red: 254/255, green: 174/255, blue: 0, alpha: 1.0)
//
//            self.appFavoriteStatus = true
//        }
//        else {
//
//            //not favorited
//            self.appFavoriteButton.setBackgroundImage(UIImage(named: "not-favorited"), for: .normal);
//            self.appFavoriteButton.setImage(UIImage(named: "gray-heart"), for: .normal);
//            self.appFavoriteButton.setTitle(" FAVORİ", for: .normal);
//            self.appFavoriteButton.setTitleColor(UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0), for: .normal);
//            self.appFavoriteButton.tintColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)
//
//            self.appFavoriteStatus = false
//
//        }
        
    }
    
    func controlApplicationDownloadStatus() {
        
//        if self.Application?.platformType == "webapps" {
//
//            self.appOpenDownloadButton.setBackgroundImage(UIImage(named:"downloadApp"), for: .normal);
//            self.appOpenDownloadButton.setTitle("AÇ", for: .normal);
//            self.appOpenDownloadButton.setTitleColor(UIColor.white, for: .normal);
//            self.appOpenDownloadButton.tintColor = UIColor.white
//
//            self.appDownloadStatus = false
//
//        }
//        else {
//
//            //native app
//
//            if MKUtilityManager.canOpenGivenUrl(urlSchema: (self.Application?.bundleId)!) {
//
//                //app is on device
//                self.appOpenDownloadButton.setBackgroundImage(UIImage(named: "downloadApp"), for: .normal);
//                self.appOpenDownloadButton.setTitle("AÇ", for: .normal);
//                self.appOpenDownloadButton.setTitleColor(UIColor.white, for: .normal);
//                self.appOpenDownloadButton.tintColor = UIColor.white
//
//                self.appDownloadStatus = false
//
//            }
//            else {
//
//                //app is not on device. downloadable
//                self.appOpenDownloadButton.setBackgroundImage(UIImage(named: "downloadApp"), for: .normal);
//                self.appOpenDownloadButton.setImage(UIImage(named:"downloadLittleIcon"), for: .normal);
//                self.appOpenDownloadButton.setTitle(" İNDİR", for: .normal);
//                self.appOpenDownloadButton.setTitleColor(UIColor.white, for: .normal);
//                self.appOpenDownloadButton.tintColor = UIColor.white
//
//                self.appDownloadStatus = true
//
//            }
//
//
//        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func setBackButtonAppearance() {
        
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.view.backgroundColor = UIColor.clear
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-white");
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-white");
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
        
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "webAppSegueFromDetail" {
            
            //let webAppViewController = segue.destination as! WebAppViewController
            //webAppViewController.Application = self.Application;
            
        }
        
    }
    
    
}

