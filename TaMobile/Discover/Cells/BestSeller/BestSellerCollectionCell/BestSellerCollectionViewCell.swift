//
//  BestSellerCollectionViewCell.swift
//
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class BestSellerCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var appBorderView: UIView!
    
    @IBOutlet weak var appImageView: UIImageView!
    
    @IBOutlet weak var appNameTextView: UILabel!
    
    @IBOutlet weak var imageLoadingIndicator: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.appBorderView.layer.cornerRadius = 50
        self.appBorderView.layer.masksToBounds = true;
        
        self.appImageView.layer.cornerRadius = 10;
        self.appImageView.layer.masksToBounds = true;
        
        self.appBorderView.layer.shadowColor = UIColor.lightGray.cgColor
        self.appBorderView.layer.shadowOffset = CGSize(width: 0, height: 5)
        self.appBorderView.layer.shadowRadius = 5
        self.appBorderView.layer.shadowOpacity = 0.3
        self.appBorderView.layer.masksToBounds = false
        self.appBorderView.layer.shadowPath = UIBezierPath(roundedRect: self.appBorderView.bounds, cornerRadius: self.appBorderView.layer.cornerRadius).cgPath
        
        
    }
    
    
}
