//
//  BestSellerTableViewCell.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

protocol ApplicationCollectionViewCellDelegate: class {
    
    //func navigateToWebApplicationFromAppSection(application:Application);
    
    //func navigateToAppDetailFromAppSection(application:Application);
    
}
class BestSellerTableViewCell: UITableViewCell, UICollectionViewDataSource, UICollectionViewDelegate {

        weak var delegate:ApplicationCollectionViewCellDelegate?

        @IBOutlet weak var bestSellerCollectionView: UICollectionView!
        //
        //        //TEMP BANNER URLS
    var bannerArray: [String] = ["https://s3-eu-west-1.amazonaws.com/developer-application-test/images/11.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/12.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/8.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/5.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/4.jpg",
                                 "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg"
        ,"https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg",
         "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/8.jpg"
    ]
        //
        //        //TheBestHunderedBooksCell
        var bestSellerCollectionViewCell: BestSellerCollectionViewCell?
        
        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code
            
            self.initCollectionViewAndRegisterCell()
        }
        
        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)
            
            // Configure the view for the selected state
        }
        
        func initCollectionViewAndRegisterCell() {
            
            //set delegate and datasource
            self.bestSellerCollectionView.delegate = self;
            self.bestSellerCollectionView.dataSource = self;
            
            //register cell
            self.bestSellerCollectionView.register(UINib(nibName: "BestSellerCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BestSellerCollectionViewCell")
            
        }
        
        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return bannerArray.count;
        }
        
        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            
            let bestSellerCollectionViewCell = self.bestSellerCollectionView.dequeueReusableCell(withReuseIdentifier: "BestSellerCollectionViewCell", for: indexPath) as? BestSellerCollectionViewCell
            
            if let url = URL(string: bannerArray[indexPath.row]) {
                
                DispatchQueue.global().async {
                    
                    DispatchQueue.main.async {
                        
                        bestSellerCollectionViewCell?.appImageView.kf.indicatorType = .activity;
                        
                        bestSellerCollectionViewCell?.appImageView.kf.setImage(with: url)
                        
                    }
                }
                
            }
            
            
            return bestSellerCollectionViewCell!
            
        }
        
        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
            
            if indexPath.row == 0 {
                
            }
            
        }
        
}

