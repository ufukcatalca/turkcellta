//
//  TheBestHunderedBooksCell.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit
import Kingfisher

    class TheBestHunderedBooksCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {

        @IBOutlet weak var theBestHunderedBooksCollectionView: UICollectionView!
//
//        //TEMP BANNER URLS
        var bannerArray: [String] = ["https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/2.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/3.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/4.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/5.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/6.jpg"
        ,"https://s3-eu-west-1.amazonaws.com/developer-application-test/images/9.jpg",
                                     "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/8.jpg"
        ]
//
//        //TheBestHunderedBooksCell
        var bestCollectionViewCell: BestCollectionViewCell?

        override func awakeFromNib() {
            super.awakeFromNib()
            // Initialization code

            self.initCollectionViewAndRegisterCell()
        }

        override func setSelected(_ selected: Bool, animated: Bool) {
            super.setSelected(selected, animated: animated)

            // Configure the view for the selected state
        }

        func initCollectionViewAndRegisterCell() {

            //set delegate and datasource
            self.theBestHunderedBooksCollectionView.delegate = self;
            self.theBestHunderedBooksCollectionView.dataSource = self;

            //register cell
            self.theBestHunderedBooksCollectionView.register(UINib(nibName: "BestCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "BestCollectionViewCell")

        }

        func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
            return bannerArray.count;
        }

        func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {

            let bestCollectionViewCell = self.theBestHunderedBooksCollectionView.dequeueReusableCell(withReuseIdentifier: "BestCollectionViewCell", for: indexPath) as? BestCollectionViewCell

            if let url = URL(string: bannerArray[indexPath.row]) {

                DispatchQueue.global().async {

                    DispatchQueue.main.async {

                        bestCollectionViewCell?.newBannerImageView.kf.indicatorType = .activity;

                        bestCollectionViewCell?.newBannerImageView.kf.setImage(with: url)

                    }
                }

            }


            return bestCollectionViewCell!

        }

        func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

            if indexPath.row == 0 {

            }

        }

}



//The Shock of the New - Robert Hughes
//The Story of Art - Ernst Gombrich (1950)
