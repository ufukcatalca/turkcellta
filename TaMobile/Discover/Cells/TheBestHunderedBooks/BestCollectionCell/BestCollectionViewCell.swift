//
//  BestCollectionViewCell.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//  Copyright © 2017. All rights reserved.
//

import UIKit

class BestCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var newBannerImageView: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.newBannerImageView.layer.cornerRadius = 10;
        self.newBannerImageView.layer.masksToBounds = true;
        
        self.layer.cornerRadius = 10;
        
        self.layer.shadowColor = UIColor.lightGray.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        self.layer.shadowRadius = 7
        self.layer.shadowOpacity = 0.7
        self.layer.masksToBounds = false
        self.layer.shadowPath = UIBezierPath(roundedRect: self.bounds, cornerRadius: self.contentView.layer.cornerRadius).cgPath
        
    }
    
}
