//
//  AppsListViewController.swift
//  asistan
//
//  Created by Horizon on 9.11.2017.
//  Copyright © 2017 Horizon All rights reserved.
//

import UIKit
import Kingfisher
import NVActivityIndicatorView

class AppsListViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, appListTableViewCellDelegate {

    var activity:NVActivityIndicatorView?
//
//    var Application:Application?
//
//    var WebApp:Application?
//
    var navBarTitle:String?
//
    @IBOutlet weak var appsListTableView: UITableView!


//    @IBOutlet weak var emptyAppState: UIImageView!

    let refreshControl = UIRefreshControl()

    public var dataManager:DataManager?
//
//    public var Applications:[Application]?
//
    public enum appListModeCase:String {

        case fromCategories = "categories"
        case fromAllApplications = "allApplications"
        case fromAllFavoriteApplications = "allFavoriteApplications"

    }

    public var appListMode:appListModeCase?

    //for categories
    public var categoryId:String?
    public var categoryName:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        //self.dataManager = DataManager.sharedDataManager

        // Do any additional setup after loading the view.

        self.appsListTableView.separatorStyle = .none
    }

    func initNavigationBarTitle(withInfo: String) {

        self.navigationItem.title = withInfo

        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 19)!]

        self.navigationController?.navigationBar.barTintColor = UIColor.white

        self.navigationController?.navigationBar.isTranslucent = true;

    }

    override func viewWillAppear(_ animated: Bool) {

        self.setBackButtonAppearance();

        activity = NVActivityIndicatorView(frame:  CGRect(x: self.view.frame.width/2 - self.view.frame.width/8, y:self.view.frame.height/2 - self.view.frame.width/8, width: self.view.frame.width/4, height: self.view.frame.width/4), type: NVActivityIndicatorType.ballScaleMultiple, color: UIColor(red: (28/255), green: (146/255), blue: (210/255), alpha: 1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)

        self.view.addSubview(activity!)

        self.activity?.startAnimating()

        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here

            switch self.appListMode {

            case .fromCategories?:

                self.navBarTitle = self.categoryName!

                //self.Applications = self.dataManager?.getAppsFromMobileCenterByCategoryId(categoryId: self.categoryId!, platformType: MKDevice.sharedMKDevice.platformType!)

                break;

            case .fromAllApplications?:

                self.navBarTitle = "UYGULAMALAR"

                //self.Applications = self.dataManager?.getAppsFromMobileCenterWithPlatform(platformType: MKDevice.sharedMKDevice.platformType!)

                break;

            case .fromAllFavoriteApplications?:

                self.navBarTitle = "FAVORİLER"

                //self.Applications = self.dataManager?.getFavoriteApplications(platformType: MKDevice.sharedMKDevice.platformType!);

                break;

            case .none:

                break;
            }

            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                // hide your progress bar here

                // Init TableView and Register Table View Cells
                //let i = self.Applications?.index(of: self.Applications![6]);

                //print("\(i!) is my girl");

                //self.initNavigationBarTitle(withInfo: self.navBarTitle!)

                self.activity?.stopAnimating()

//                if self.Applications?.count == 0 {
//
//                    self.emptyAppState.image = UIImage(named: "appEmpty");
//
//                    self.appsListTableView.isHidden = true;
//
//                    self.emptyAppState.isHidden = false;
//
//                }
//                else {
//
//                    self.appsListTableView.isHidden = false;
//
//                    self.emptyAppState.isHidden = true;
//
//                    self.initTableViewAndRegisterCells();
//
//                    self.appsListTableView.reloadData();
//
//                }

            }
        }

    }


    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    func setBackButtonAppearance() {

        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.view.backgroundColor = UIColor.white

        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);


    }


    func initTableViewAndRegisterCells() {

        //set delegate and datasource
        self.appsListTableView.delegate = self;
        self.appsListTableView.dataSource = self;

        //set separator style
        self.appsListTableView.separatorStyle = UITableViewCellSeparatorStyle.none;

        //register custom cell
        self.appsListTableView.register(UINib(nibName: "AppTableViewCell", bundle: nil), forCellReuseIdentifier: "appTableViewCellId");


        if #available(iOS 10, *) {

            self.appsListTableView.refreshControl = self.refreshControl

        } else {

            self.appsListTableView.addSubview(self.refreshControl)

        }

        self.refreshControl.addTarget(self, action: #selector(refreshApplicationsList(_:)), for: .valueChanged)

        self.refreshControl.tintColor = UIColor.init(red: 103/255, green: 122/255, blue:152/255, alpha: 1.0);

        let multipleRefreshControlAttributes:[NSAttributedStringKey:Any] = [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 15)!,
                                                                            NSAttributedStringKey.foregroundColor : UIColor.init(red: 103/255, green: 122/255, blue:152/255, alpha: 1.0)]

        self.refreshControl.attributedTitle = NSAttributedString(string: "Uygulamalar güncelleniyor...", attributes: multipleRefreshControlAttributes);

    }

    @objc func refreshApplicationsList(_ sender:Any){

        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here

            switch self.appListMode {

            case .fromCategories?:

                self.navBarTitle = self.categoryName!

                //self.Applications = self.dataManager?.getAppsFromMobileCenterByCategoryId(categoryId: self.categoryId!, platformType: MKDevice.sharedMKDevice.platformType!)

                break;

            case .fromAllApplications?:

                self.navBarTitle = "UYGULAMALAR"

                //self.Applications = self.dataManager?.getAppsFromMobileCenterWithPlatform(platformType: MKDevice.sharedMKDevice.platformType!)

                break;

            case .fromAllFavoriteApplications?:

                self.navBarTitle = "FAVORİLER"

                //self.Applications = self.dataManager?.getFavoriteApplications(platformType: MKDevice.sharedMKDevice.platformType!);

                break;

            case .none:

                break;
            }

            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                // hide your progress bar here

                self.refreshControl.endRefreshing()

                self.appsListTableView.reloadData();

            }
        }

    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        //return (self.Applications?.count)!;
        return 1
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120;
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var appTableViewCell = self.appsListTableView.dequeueReusableCell(withIdentifier: "appTableViewCellId", for: indexPath) as? AppTableViewCell;

        if appTableViewCell == nil {

            appTableViewCell = AppTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "appTableViewCellId");

        }

        //appTableViewCell?.delegate = self;

//        let application = self.Applications![indexPath.row];
//
//        appTableViewCell?.Application = application;
//        appTableViewCell?.appNameLabel.text = application.name;
//        appTableViewCell?.appVersionLabel.text = application.version
//
//        var platformImage:UIImage?
//
//        if application.platformType == "webapps" {
//
//            platformImage = UIImage(named: "browser")
//
//        }
//        else {
//
//            platformImage = UIImage(named: "cellPhone")
//
//        }
//
//        appTableViewCell?.appPlatformImageView.image = platformImage;
//
//
//        if let url = URL(string: application.appIconUrl!) {
//
//            DispatchQueue.global().async {
//
//                DispatchQueue.main.async {
//
//                    appTableViewCell?.appIconImageView.kf.indicatorType = .activity;
//
//                    appTableViewCell?.appIconImageView.kf.setImage(with: url)
//
//                }
//            }
//        }
//
//        //appTableViewCell?.controlApplicationFavoriteStatus()
//        //appTableViewCell?.controlApplicationDownloadStatus()

        return appTableViewCell!;

    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {

//        if let mApplication = self.Applications?[indexPath.row] {
//
//            self.Application = mApplication;
//
//            self.performSegue(withIdentifier: "appDetailSegue", sender: self);
//
//        }

    }

//    func navigateToWebApplication(application: Application) {
//
//        self.WebApp = application;
//
//        self.performSegue(withIdentifier: "webAppSegueFromList", sender: self);
//
//    }


    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {

        /*cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)

         UIView.animate(withDuration: 0.3, animations: {
         cell.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1)
         },completion: { finished in
         UIView.animate(withDuration: 0.1, animations: {
         cell.layer.transform = CATransform3DMakeScale(1.03, 1.03, 1)
         })
         })
         */

    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.

        if segue.identifier == "appDetailSegue" {

            let appDetailViewController = segue.destination as! AppDetailViewController

            //appDetailViewController.Application = self.Application;

        }
        else if segue.identifier == "webAppSegueFromList" {

            //let webAppViewController = segue.destination as! WebAppViewController

            //webAppViewController.Application = self.WebApp

        }

    }
    
    
}

