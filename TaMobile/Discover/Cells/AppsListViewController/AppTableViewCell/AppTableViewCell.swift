//
//  AppTableViewCell.swift
//
//
//  Created by Horizon on 16.12.2017.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import NVActivityIndicatorView

protocol appListTableViewCellDelegate: class {
    
    //func navigateToWebApplication(application:Application);
    
}

class AppTableViewCell: UITableViewCell {
    
//    weak var delegate:appListTableViewCellDelegate?
//
//    let dataManager = DataManager.sharedDataManager
//
//    var activity:NVActivityIndicatorView?
//
//    @IBOutlet weak var appIconImageView: UIImageView!
//
//    @IBOutlet weak var appPlatformImageView: UIImageView!
//
//    @IBOutlet weak var appNameLabel: UILabel!
//
//    @IBOutlet weak var appVersionLabel: UILabel!
//
//    @IBOutlet weak var favAppButton: UIButton!
//
//    @IBOutlet weak var openDownloadAppButton: UIButton!
//
//    @IBOutlet weak var borderView: UIView!
//
//
//
//    //Application holds application object
//
//    var Application:Application?
//
//    var appFavoriteStatus:Bool?
//    var appDownloadStatus:Bool?
//
//    @IBAction func favoriteApplication(_ sender: Any) {
//
//        activity = NVActivityIndicatorView(frame:  CGRect(x: self.frame.width/2 - self.frame.width/8, y:self.frame.height/2 - self.frame.width/8, width: self.frame.width/4, height: self.frame.width/4), type: NVActivityIndicatorType.ballScaleMultiple, color: UIColor.orange, padding: NVActivityIndicatorView.DEFAULT_PADDING)
//
//        self.addSubview(activity!)
//
//        self.activity?.startAnimating()
//
//        DispatchQueue.global(qos: .background).async {
//            // Background thread:
//            // start loading your images here
//
//            if self.appFavoriteStatus == false {
//
//                if self.dataManager.setApplicationAsFavoriteStatus(appId: (self.Application?.id)!, unfavorite: false) {
//
//                    //app is favorited right now
//                    self.Application?.isFavorite = true;
//
//                }
//
//            }
//            else {
//
//                if self.dataManager.setApplicationAsFavoriteStatus(appId: (self.Application?.id)!, unfavorite: true) {
//
//                    //app is unfavorited right now
//                    self.Application?.isFavorite = false;
//
//
//                }
//
//            }
//
//            DispatchQueue.main.async {
//                // Main thread, called after the previous code:
//                // hide your progress bar here
//
//                // Init TableView and Register Table View Cells
//
//                self.activity?.stopAnimating()
//
//                self.controlApplicationFavoriteStatus()
//
//            }
//
//        }
//
//
//    }
//
//
//    @IBAction func downloadApplication(_ sender: Any) {
//
//        if Application?.platformType == "webapps" {
//
//            self.delegate?.navigateToWebApplication(application: self.Application!)
//
//        }
//        else {
//
//            if appDownloadStatus == false {
//
//                MKUtilityManager.openAppWithURLSchema(urlSchema: (self.Application?.bundleId)!, userId: MKUser.sharedMKUser.id!, userPassword: MKUser.sharedMKUser.password!)
//
//            }
//            else {
//
//                if let appPathUrl = self.Application?.appFilePath {
//
//                    MKDownloadManager.downloadIpaFromPlistUrl(plistUrl: appPathUrl)
//
//                }
//
//            }
//
//        }
//
//    }
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        // Initialization code
//
//        self.appIconImageView.layer.masksToBounds = true;
//        self.appIconImageView.layer.cornerRadius = 10;
//
//        self.borderView.layer.cornerRadius = 10;
//        self.borderView.layer.masksToBounds = false;
//        self.borderView.layer.shadowColor = UIColor.lightGray.cgColor
//        self.borderView.layer.shadowOffset = CGSize(width: self.borderView.bounds.width*0.05, height: 4.0)
//        self.borderView.layer.shadowRadius = 10
//        self.borderView.layer.shadowOpacity = 0.6
//        self.borderView.layer.masksToBounds = false
//        self.borderView.layer.shadowPath = UIBezierPath(roundedRect: self.borderView.bounds, cornerRadius: self.borderView.layer.cornerRadius).cgPath
//
//    }
//
//    func controlApplicationDownloadStatus() {
//
//        if self.Application?.platformType == "webapps" {
//
//            self.openDownloadAppButton.setBackgroundImage(UIImage(named:"downloadApp"), for: .normal);
//            self.openDownloadAppButton.setTitle("AÇ", for: .normal);
//            self.openDownloadAppButton.setTitleColor(UIColor.white, for: .normal);
//            self.openDownloadAppButton.tintColor = UIColor.white
//
//            self.appDownloadStatus = false // not downloadable only open
//
//        }
//        else {
//
//            //native app
//
//            if MKUtilityManager.canOpenGivenUrl(urlSchema: (self.Application?.bundleId)!) {
//
//                //app is on device
//                self.openDownloadAppButton.setBackgroundImage(UIImage(named: "downloadApp"), for: .normal);
//                self.openDownloadAppButton.setTitle("AÇ", for: .normal);
//                self.openDownloadAppButton.setTitleColor(UIColor.white, for: .normal);
//                self.openDownloadAppButton.tintColor = UIColor.white
//
//                self.appDownloadStatus = false // not downloadable only open
//
//            }
//            else {
//
//                //app is not on device. downloadable
//                self.openDownloadAppButton.setBackgroundImage(UIImage(named: "downloadApp"), for: .normal);
//                self.openDownloadAppButton.setImage(UIImage(named:"downloadLittleIcon"), for: .normal);
//                self.openDownloadAppButton.setTitle(" İNDİR", for: .normal);
//                self.openDownloadAppButton.setTitleColor(UIColor.white, for: .normal);
//                self.openDownloadAppButton.tintColor = UIColor.white
//
//                self.appDownloadStatus = true
//
//            }
//
//
//        }
//
//    }
//
//    func controlApplicationFavoriteStatus() {
//
//        if self.Application?.isFavorite == true {
//
//            //favorited
//            self.favAppButton.setBackgroundImage(UIImage(named: "favorited"), for: .normal);
//            self.favAppButton.setImage(UIImage(named: "orange-heart"), for: .normal);
//            self.favAppButton.setTitle(" FAVORİ", for: .normal);
//            self.favAppButton.setTitleColor(UIColor(red: 254/255, green: 174/255, blue: 0, alpha: 1.0), for: .normal);
//            self.favAppButton.tintColor = UIColor(red: 254/255, green: 174/255, blue: 0, alpha: 1.0)
//
//            self.appFavoriteStatus = true // is favorited
//        }
//        else {
//
//            //not favorited
//            self.favAppButton.setBackgroundImage(UIImage(named: "not-favorited"), for: .normal);
//            self.favAppButton.setImage(UIImage(named: "gray-heart"), for: .normal);
//            self.favAppButton.setTitle(" FAVORİ", for: .normal);
//            self.favAppButton.setTitleColor(UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0), for: .normal);
//            self.favAppButton.tintColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0)
//
//            self.appFavoriteStatus = false // is not favorited
//
//        }
//
//    }
//
//    override func setSelected(_ selected: Bool, animated: Bool) {
//        super.setSelected(selected, animated: animated)
//
//        // Configure the view for the selected state
//    }
    
}

