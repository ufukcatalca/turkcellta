//
//  SearchTableViewCell.swift
// 
//
//  Created by Ufuk Catalca on 12/5/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit

class SearchTableViewCell: UITableViewCell {
    
    @IBOutlet weak var searchAppIconImageView: UIImageView!
    
    @IBOutlet weak var searchAppPlatformImageView: UIImageView!
    
    @IBOutlet weak var searchAppCategoryLabel: UILabel!
    
    @IBOutlet weak var searchAppNameLabel: UILabel!
    
    @IBOutlet weak var borderView: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        self.searchAppIconImageView.layer.masksToBounds = true;
        self.searchAppIconImageView.layer.cornerRadius = 10;
        
    }
    
    override func layoutSubviews() {
        
        super.layoutSubviews()
        
        self.borderView.layer.cornerRadius = 10;
        self.borderView.layer.masksToBounds = false;
        self.borderView.layer.shadowColor = UIColor.lightGray.cgColor
        self.borderView.layer.shadowOffset = CGSize(width: self.borderView.bounds.width*0.05, height: 4.0)
        self.borderView.layer.shadowRadius = 10
        self.borderView.layer.shadowOpacity = 0.6
        self.borderView.layer.masksToBounds = false
        self.borderView.layer.shadowPath = UIBezierPath(roundedRect: self.borderView.bounds, cornerRadius: self.borderView.layer.cornerRadius).cgPath
        
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
    }
    
}
