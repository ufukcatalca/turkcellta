//
//  SearchViewController.swift
//
//  Created by Ufuk Catalca on 12/5/17.
//  Copyright © 2017 . All rights reserved.
//

import UIKit
import Kingfisher

class SearchViewController: UIViewController, UISearchBarDelegate, UITableViewDelegate, UITableViewDataSource {
    
    
    @IBOutlet weak var searchTableView: UITableView!
    
    @IBOutlet weak var searchBar: UISearchBar!
    
    //let dataManager:DataManager = DataManager.sharedDataManager
    
    var searchTextArgument:String?
    
    var userSearchText:String?
    
    
    var selectedSearchAppId:String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        //self.getSearchedApplications(withSearchText: self.searchTextArgument!)
        
        setSearchBarAppearence()
        
        initTableViewAndRegisterCells()
        
        
    }
    
    func getSearchedApplications(withSearchText:String) {
        
//        self.searchedApplications = dataManager.searchApplicationWithPlatformTypeAndTerm(platformType: MKDevice.sharedMKDevice.platformType!, term: withSearchText)
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.initNavigationBarTitle(withInfo: "ARAMA SONUÇLARI")
        
    }
    
    func initNavigationBarTitle(withInfo: String) {
        
        self.navigationItem.title = withInfo
        
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 19)!]
        
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        
        self.navigationController?.navigationBar.isTranslucent = true;
        
        self.navigationController?.navigationBar.setBackgroundImage(nil, for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = nil
        self.navigationController?.view.backgroundColor = UIColor.white
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidBeginEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidEndEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("searchBarTextDidChange: \(searchText)");
        
        self.userSearchText = searchText
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("SearchButtonClicked");
        
        self.searchBar.resignFirstResponder()
        
        self.getSearchedApplications(withSearchText: self.userSearchText!)
        
        self.searchTableView.reloadData()
        
        
    }
    
    func setSearchBarAppearence() {
        
        //Delegate
        self.searchBar.delegate = self;
        
        //searchbar layer
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        searchBar.layer.shadowColor = UIColor.lightGray.cgColor
        searchBar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        searchBar.layer.shadowRadius = 10
        searchBar.layer.shadowOpacity = 0.6
        searchBar.layer.masksToBounds = false
        searchBar.layer.shadowPath = UIBezierPath(roundedRect: self.searchBar.bounds, cornerRadius: self.view.layer.cornerRadius).cgPath
        
        //Textfield in searchbar
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        
        //searchbar left item set color
        let leftItem = textField.leftView as! UIImageView
        leftItem.image = leftItem.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        leftItem.tintColor = UIColor(red: (144/255), green: (194/255), blue: (35/255), alpha: 1.0)
        
    }
    
    func initTableViewAndRegisterCells() {
        
        //set delegate and datasource
        self.searchTableView.delegate = self;
        self.searchTableView.dataSource = self;
        
        //set separator style
        self.searchTableView.separatorStyle = UITableViewCellSeparatorStyle.none;
        
        //register custom cell
        self.searchTableView.register(UINib(nibName: "SearchTableViewCell", bundle: nil), forCellReuseIdentifier: "searchTableViewCellId");
        
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        //return (self.searchedApplications?.count)!;
        
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 120;
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        var searchCell = self.searchTableView.dequeueReusableCell(withIdentifier: "searchTableViewCellId", for: indexPath) as? SearchTableViewCell
        
        if searchCell == nil {
            
            searchCell = SearchTableViewCell(style:.default, reuseIdentifier: "searchTableViewCellId");
            
        }
        
//
//        searchCell?.searchAppNameLabel.text = searchedApplication.name
//        searchCell?.searchAppCategoryLabel.text = searchedApplication.category
        
        var platformImage:UIImage?
        
//        if searchedApplication.platformType == "webapps" {
//
//            platformImage = UIImage(named: "browser")
//
//        }
//        else {
//
//            platformImage = UIImage(named: "cellPhone")
//
//        }
        
        searchCell?.searchAppPlatformImageView.image = platformImage
        
//        if let url = URL(string: searchedApplication.appIconUrl!) {
//
//            DispatchQueue.global().async {
//
//                DispatchQueue.main.async {
//
//                   // searchCell?.searchAppIconImageView.kf.indicatorType = .activity;
//
//                    //searchCell?.searchAppIconImageView.kf.setImage(with: url)
//
//                }
//            }
//        }
        
        return searchCell!
        
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        //let application:Application = self.searchedApplications![indexPath.row]
        
//        let appId = application.id
//
//        self.selectedSearchAppId = appId
        
        self.performSegue(withIdentifier: "searchToDetailSegue", sender: self)
        
    }
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "searchToDetailSegue" {
            
            //let appDetailViewController:AppDetailViewController = segue.destination as! AppDetailViewController
            
           // appDetailViewController.searchedAppId = self.selectedSearchAppId
            
        }
        
    }
    
    
}

