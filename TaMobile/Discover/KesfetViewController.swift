//
//  KesfetViewController.swift
//
//  Created by Ufuk Catalca on 12/4/17.
//
import UIKit
import NVActivityIndicatorView
import UserNotifications

class KesfetViewController: UIViewController,UITableViewDelegate,UITableViewDataSource, UISearchBarDelegate, UNUserNotificationCenterDelegate,ApplicationCollectionViewCellDelegate, FavoriteAppCollectionViewCellDelegate  {
    
    
    @IBOutlet weak var homePageTableView: UITableView!
    
    
    //dataManager
    let dataManager = DataManager.sharedInstance
    
    //refresh control
    let refreshControl = UIRefreshControl()
    
    //table view custom cells
    var theBestHunderedBooksCell: TheBestHunderedBooksCell?  // newsTableViewCell
    var newBooksTableViewCell: NewBooksTableViewCell?        // favoritesTableViewCell
    var bestSellerTableViewCell: BestSellerTableViewCell?    // applicationsTableViewCell
    var emptyTableViewCell: EmptyTableViewCell?        // empty
    
    
    //NVActivityIndicatorView
    var activity: NVActivityIndicatorView?
    
    //applicationsListEmpty
    var applicationsListEmpty:Bool = true
    //favoriteApplicationsListEmptyappListMode
    var favApplicationsListEmpty:Bool = true
    
    
    //search bar
    @IBOutlet weak var searchBar: UISearchBar!
    
    //search text
    var userSearchText:String?
    
    //list mode
    //public var appListMode: AppsListViewController.appListModeCase?
    
    //table view
    @IBOutlet weak var kesfetPageTableView: UITableView!
    
    // news // favori // uygulamalar
    // en iyi 100 kitap   // yeni çıkanlar  // en çok satanlar
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = "KEŞFET"
        
        for i:Int in 0..<(self.tabBarController?.view.subviews.count)! {
            
            if self.tabBarController?.view.subviews[i].tag == 1 {
                
                self.tabBarController?.view.subviews[i].isHidden = false
                //self.tabBarController?.view.subviews[i].removeFromSuperview();
                
                break;
                
            }
        }
        
        let notificationCenter = UNUserNotificationCenter.current()
        notificationCenter.delegate = self;
        notificationCenter.requestAuthorization(options: [.alert,.badge,.sound]) { (granted, error) in
            
            if granted {
                
                print("notification center request authorization granted");
                
            }
            
        }
        
        UIApplication.shared.registerForRemoteNotifications()
        
        setBackButtonAppearance()
        
        initSwipeDownGesture()
        
        self.homePageTableView.separatorStyle = .none
        
        
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        
        self.navigationItem.title = "KEŞFET";
        
        // Do any additional setup after loading the view.
        setSearchBarAppearence()
        
        self.homePageTableView.reloadData()
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        
        self.setViewUpDownWithKeyboardTogglesRemoveObserver()
        
        self.navigationController?.isNavigationBarHidden = false
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.searchBar.text = "";
        self.userSearchText = "";
        
        self.navigationController?.isNavigationBarHidden = true
        
        activity = NVActivityIndicatorView(frame:  CGRect(x: self.view.frame.width/2 - self.view.frame.width/8, y:self.view.frame.height/2 - self.view.frame.width/8, width: self.view.frame.width/4, height: self.view.frame.width/4), type: NVActivityIndicatorType.ballScaleMultiple, color: UIColor.orange, padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        self.view.addSubview(activity!)
        
        self.activity?.startAnimating()
        
        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here
            
            //self.Applications = self.dataManager.getAppsFromMobileCenterWithPlatform(platformType: MKDevice.sharedMKDevice.platformType!)
            
            //self.FavApplications = self.dataManager.getFavoriteApplications(platformType: MKDevice.sharedMKDevice.platformType!)
            
            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                // hide your progress bar here
                
                // Init TableView and Register Table View Cells
                
                //  self.favApplicationsListEmpty = self.isFavoriteApplicationListEmpty(favAppsList: self.FavApplications)
                
                //self.applicationsListEmpty = self.isApplicationListEmpty(appsList: self.Applications)
                
                self.activity?.stopAnimating()
                
                self.initTableViewAndRegisterCells()
                
                self.kesfetPageTableView.reloadData()
                
            }
        }
        
    }
    
    func setViewUpDownWithKeyboardTogglesRemoveObserver() {
        
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.removeObserver(self, name: Notification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
    func initSwipeDownGesture() {
        
        let swipeDownGesture = UISwipeGestureRecognizer(target: self, action: #selector(searchResignFirstResponder(sender:)));
        swipeDownGesture.direction = .down
        self.view.isUserInteractionEnabled = true;
        self.view.addGestureRecognizer(swipeDownGesture);
        
    }
    
    
    //keyboard notifications
    @objc func keyboardWillShow(notification: NSNotification) {
        
        self.homePageTableView.isScrollEnabled = false;
        
        if #available(iOS 10.0, *) {
            
            self.homePageTableView.refreshControl = nil;
            
        }
        else {
            
            self.homePageTableView.refreshControl?.removeFromSuperview()
            
        }
        
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        
        self.homePageTableView.isScrollEnabled = true;
        
        if #available(iOS 10.0, *) {
            
            self.homePageTableView.refreshControl = self.refreshControl;
            
        }
        else {
            
            self.homePageTableView.addSubview(self.refreshControl);
        }
    }
    
    func setBackButtonAppearance() {
        
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 3
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        
        switch (indexPath.section) {
            
        case 0:
            return 160
            
        case 1:
            return 150
            
        case 2:
            return 170
            
        default:
            return 0
        }
        
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        
        switch (section) {
        case 0:
            return 0
        case 1:
            return 30
        case 2:
            return 30
        default:
            return 30
        }
        
    }
    
    
    
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        
        let headerWidth:CGFloat = self.homePageTableView.frame.size.width
        let headerHeight:CGFloat = 30
        
        let headerLabel = UILabel(frame: CGRect(x: 13, y: 4, width: 200, height: 25))
        headerLabel.font = UIFont(name: "Helvetica Neue", size: 20)
        headerLabel.textColor = UIColor(red: 31/255, green: 49/255, blue: 74/255, alpha: 1.0)
        
        let headerView = UIView(frame: CGRect(x: 0, y: 0, width: headerWidth, height: headerHeight))
        headerView.backgroundColor = UIColor(red: 243/255, green: 245/255, blue: 248/255, alpha: 1.0)
        
        headerView.addSubview(headerLabel)
        
        switch (section) {
            
        case 0:
            
            let allFavAppsLabel = UILabel(frame: CGRect(x: headerWidth - 65, y: 6, width: 100, height: 25));
            allFavAppsLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 15)
            allFavAppsLabel.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0);
            
            let tapOnFavAllApssLabel = UITapGestureRecognizer(target: self, action: #selector(tapOnAllFavAppsLabel(sender:)));
            allFavAppsLabel.isUserInteractionEnabled = true;
            allFavAppsLabel.addGestureRecognizer(tapOnFavAllApssLabel);
            
            headerView.addSubview(allFavAppsLabel);
            headerLabel.text = "100 Temel Eser"
            allFavAppsLabel.text = "TÜMÜ"
            
            allFavAppsLabel.isHidden = false
            
            return headerView
        case 1:
            
            let allFavAppsLabel = UILabel(frame: CGRect(x: headerWidth - 65, y: 6, width: 100, height: 25));
            allFavAppsLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 15)
            allFavAppsLabel.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0);
            
            let tapOnFavAllApssLabel = UITapGestureRecognizer(target: self, action: #selector(tapOnAllFavAppsLabel(sender:)));
            allFavAppsLabel.isUserInteractionEnabled = true;
            allFavAppsLabel.addGestureRecognizer(tapOnFavAllApssLabel);
            
            headerView.addSubview(allFavAppsLabel);
            headerLabel.text = "Yeni Çıkanlar"
            allFavAppsLabel.text = "TÜMÜ"
            
            
            allFavAppsLabel.isHidden = false
            
            return headerView
            
        case 2:
            
            let allAppsLabel = UILabel(frame: CGRect(x: headerWidth - 65, y: 6, width: 100, height: 25));
            allAppsLabel.font = UIFont(name: "HelveticaNeue-Regular", size: 15)
            allAppsLabel.textColor = UIColor(red: 155/255, green: 155/255, blue: 155/255, alpha: 1.0);
            
            //tap on allAppsLabel
            let tapOnAllApssLabel = UITapGestureRecognizer(target: self, action: #selector(tapOnAllAppsLabel(sender:)));
            allAppsLabel.isUserInteractionEnabled = true;
            allAppsLabel.addGestureRecognizer(tapOnAllApssLabel);
            
            
            headerView.addSubview(allAppsLabel)
            headerLabel.text = "En Çok Satanlar"
            allAppsLabel.text = "TÜMÜ"
            
            allAppsLabel.isHidden = false
            
            return headerView;
            
        default:
            return UIView();
        }
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        switch (indexPath.section) {
            
        case 0:
            
            //TheBestHunderedBooksCell
            self.theBestHunderedBooksCell = self.kesfetPageTableView.dequeueReusableCell(withIdentifier: "TheBestHunderedBooksCell", for: indexPath) as? TheBestHunderedBooksCell
            
            if self.theBestHunderedBooksCell == nil {
                
                self.theBestHunderedBooksCell = TheBestHunderedBooksCell(style: UITableViewCellStyle.default, reuseIdentifier: "TheBestHunderedBooksCell")
                
            }
            
            return self.theBestHunderedBooksCell!
            
        case 1:
            //NewBooksTableViewCell
            
            self.newBooksTableViewCell = self.kesfetPageTableView.dequeueReusableCell(withIdentifier: "NewBooksTableViewCell", for: indexPath) as? NewBooksTableViewCell
            
            if newBooksTableViewCell == nil {
                
                self.newBooksTableViewCell = NewBooksTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "NewBooksTableViewCell")
                
            }
            return newBooksTableViewCell!
            
        case 2:
            
            //BestSellerTableViewCell
            
            self.bestSellerTableViewCell = self.kesfetPageTableView.dequeueReusableCell(withIdentifier: "BestSellerTableViewCell", for: indexPath) as? BestSellerTableViewCell
            
            if bestSellerTableViewCell == nil {
                
                self.bestSellerTableViewCell = BestSellerTableViewCell(style: UITableViewCellStyle.default, reuseIdentifier: "BestSellerTableViewCell")
                
            }
            
            
            return bestSellerTableViewCell!
            
        default:
            
            return UITableViewCell()
            
        }
        
        
    }
    
    
    //init tableview and register cells
    func initTableViewAndRegisterCells() {
        
        //delegate and datasource
        self.kesfetPageTableView.delegate = self;
        self.kesfetPageTableView.dataSource = self;
        
        //set cell separator style
        self.kesfetPageTableView.separatorStyle = UITableViewCellSeparatorStyle.none
        
        //register custom cells
        
        //newsTableViewCell
        self.kesfetPageTableView.register(UINib(nibName: "TheBestHunderedBooksCell", bundle: nil), forCellReuseIdentifier: "TheBestHunderedBooksCell")
        
        //favoritesTableViewCell
        self.kesfetPageTableView.register(UINib(nibName: "NewBooksTableViewCell", bundle: nil), forCellReuseIdentifier: "NewBooksTableViewCell")
        
        //applicationsTableViewCell
        self.kesfetPageTableView.register(UINib(nibName: "BestSellerTableViewCell", bundle: nil) , forCellReuseIdentifier: "BestSellerTableViewCell")
        
        
        if #available(iOS 10.0, *) {
            
            self.kesfetPageTableView.refreshControl = self.refreshControl;
            
        }
        else {
            
            self.kesfetPageTableView.addSubview(self.refreshControl);
            
        }
        
        self.refreshControl.addTarget(self, action: #selector(refreshHomePageView(_:)), for: .valueChanged)
        
        self.refreshControl.tintColor = UIColor(red: 144/255, green: 194/255, blue: 35/255, alpha: 1.0);
        
        let multipleRefreshControlAttributes:[NSAttributedStringKey:Any] = [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 15)!,
                                                                            NSAttributedStringKey.foregroundColor : UIColor(red: 144/255, green: 194/255, blue: 35/255, alpha: 1.0)]
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "Keşfet Anasayfa güncelleniyor...", attributes: multipleRefreshControlAttributes);
        
    }
    
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("SearchButtonClicked");
        
        self.searchBar.resignFirstResponder()
        
        self.performSegue(withIdentifier: "searchSegue", sender: self);
        
        
    }
    
    
    
    @objc private func refreshHomePageView(_ sender:Any) {
        
        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here
            
            //self.Applications = self.dataManager.getAppsFromMobileCenterWithPlatform(platformType: MKDevice.sharedMKDevice.platformType!)
            
            //self.FavApplications = self.dataManager.getFavoriteApplications(platformType: MKDevice.sharedMKDevice.platformType!)
            
            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                // hide your progress bar here
                
                //  self.favApplicationsListEmpty = self.isFavoriteApplicationListEmpty(favAppsList: self.FavApplications)
                
                //self.applicationsListEmpty = self.isApplicationListEmpty(appsList: self.Applications)
                
                self.refreshControl.endRefreshing()
                
                self.kesfetPageTableView.reloadData()
                
            }
        }
        
    }
    
    
    
    
    @objc func searchResignFirstResponder(sender: UISwipeGestureRecognizer) {
        
        self.searchBar.resignFirstResponder();
        
    }
    
    func setSearchBarAppearence() {
        
        //Delegate
        self.searchBar.delegate = self;
        
        //searchbar layer
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        searchBar.layer.shadowColor = UIColor.lightGray.cgColor
        searchBar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        searchBar.layer.shadowRadius = 10
        searchBar.layer.shadowOpacity = 0.6
        searchBar.layer.masksToBounds = false
        searchBar.layer.shadowPath = UIBezierPath(roundedRect: self.searchBar.bounds, cornerRadius: self.view.layer.cornerRadius).cgPath
        
        //Textfield in searchbar
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        
        //searchbar left item set color
        let leftItem = textField.leftView as! UIImageView
        leftItem.image = leftItem.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        leftItem.tintColor = UIColor(red: (144/255), green: (194/255), blue: (35/255), alpha: 1.0)
        
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidBeginEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidEndEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("searchBarTextDidChange: \(searchText)");
        
        self.userSearchText = searchText
        
    }
    
    
    
    @objc func tapOnAllFavAppsLabel(sender: UITapGestureRecognizer){
        
        print("TAPTAPTAPFAV")
        
        //appListMode = AppsListViewController.appListModeCase(rawValue: "allFavoriteApplications");
        
        self.performSegue(withIdentifier: "appListContentSegue", sender: self);
        
    }
    
    @objc func tapOnAllAppsLabel(sender: UITapGestureRecognizer){
        
        print("TAPTAPTAP")
        
        //  appListMode = AppsListViewController.appListModeCase(rawValue: "allApplications");
        
        self.performSegue(withIdentifier: "appListContentSegue", sender: self);
        
    }
    
    
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "appListContentSegue" {
            
            let appListViewController:AppsListViewController = segue.destination as! AppsListViewController;
            //appListViewController.appListMode = self.appListMode
            
        }
        else if segue.identifier == "searchSegue" {
            
            let searchViewController:SearchViewController = segue.destination as! SearchViewController
            
            searchViewController.searchTextArgument = self.userSearchText
            
            self.userSearchText = ""
            
        }
        else if segue.identifier == "webAppSegueFromHome" {
            
            //            let webAppViewController:WebAppViewController = segue.destination as! WebAppViewController
            //
            //            webAppViewController.Application = self.webApplicaiton
            
        }
        else if segue.identifier == "detailSegueFromHome" {
            
            let detailViewController:AppDetailViewController = segue.destination as! AppDetailViewController
            
            //detailViewController.Application = self.mApplication
            
        }
        
    }
}
