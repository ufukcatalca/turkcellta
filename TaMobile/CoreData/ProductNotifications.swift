//
//  UserNotification.swift
//
import UIKit
import CoreData
import Foundation
class ProductNotifications: NSManagedObject {
    
    var product_id: String?
    var name: String?
    var price: TimeInterval?
    var images: String?
    var descriptions: String?
}
