//
//  DataManager.swift
//
import Foundation
import UIKit
//import AwaitKit
import Alamofire

class DataManager: NSObject {
    
    public var productsArr = [Products]()
    //public var productsApp:[Products]?
    
    var selectedProduct_id: [String: String] = [:]
    
    public static let sharedInstance: DataManager = {
        
        let instance = DataManager()
        instance.productsArr = []
        return instance
        
    }()
    
    override private init() {
        super.init()
    }
    
    
    func getAllProductList()->GenericResponse? {
        
        let productId : String = "" // self.seciliMagaza["product_id"] as! String
        
        let params: Dictionary = ["product_id":productId,] as Dictionary
        
        if let response : GenericResponse =  FetchManager.getAllProductList(params: params){
            
            //if response.status == "OK" {
            
            for dic in response.dataArr {
                let product : Products?
                
                /*
                 {
                 "product_id" : "1",
                 "name" : "Apples",
                 "price" : 120,
                 "image" : "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg"
                 },
                 
                 */
                
                let product_id = (dic as AnyObject)["product_id"] as! String
                // "product_id": "1"
                
                let name = (dic as AnyObject).value(forKey: "name") as! String
                // "name": "Apples"
                
                let price = (dic as AnyObject).value(forKey: "price") as! Int
                // "price": "120",
                
                let images = (dic as AnyObject).value(forKey: "image") as! String
                
                //image" : "https://s3-eu-west-1.amazonaws.com/developer-application-test/images/1.jpg"
                
                
                product = Products(product_id: product_id, name: name, price: price, images: images,descriptions:"")
                
                self.productsArr.append(product!)
                
            }
            //}
            return response
        }
        else {
            return nil
        }
        
    }
    
    
    func getProductDetail(product_id:String)-> String? {
        
        let productId : String = self.selectedProduct_id["product_id"] as! String
        
        let params: Dictionary = ["product_id":productId] as Dictionary
        
        if let response : GenericResponse =  FetchManager.getProductDetail(params: params){
            
            let descriptions = response.dataDict["description"] as! String
            // "description" : "An apple a day keeps the doctor away."
            
            return descriptions
        }
        else {
            return nil
        }
        
    }
    
}
