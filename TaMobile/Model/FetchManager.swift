//
//  FetchManager.swift
//

//#define cardList = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/list"
//#define CardItemDetail = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart/1/detail"

import Foundation
import UIKit
//import AwaitKit
//import PromiseKit
import Alamofire

class FetchManager: NSObject {
    
    static let cartApiUrl = "https://s3-eu-west-1.amazonaws.com/developer-application-test/cart"
    
    var resDict = Dictionary<String, Any>() //Array of dictionary
    
    class func getAllProductList(params:Dictionary<String, Any>)->GenericResponse? {
        
        let urlStr = String(format:"%@/list",cartApiUrl)
        
        let callAddress:URL = URL(string: urlStr)!
        
        if let data : Data = self.executeRequestWithUrlNew(url: callAddress as URL, method: "get", params: params){
            
            return self.parseCollectionResult(data: data)
            
        }else {
            return nil
        }
        
    }
    
    
    class func getProductDetail(params:Dictionary<String, Any>)->GenericResponse? {
        
        let product_id : NSString = params["product_id"] as! NSString
        let urlStr = String(format:"%@/%@/detail",cartApiUrl,product_id)
        
        let callAddress:URL = URL(string: urlStr)!
        
        if let data : Data = self.executeRequestWithUrlNew(url: callAddress as URL, method: "get", params: params){
            
            return self.parseObjectResult(data: data)
            
        }else {
            return nil
        }
    }
    
    
    
    
    class func executeRequestWithUrlNew(url : URL,method: String,params:Dictionary<String, Any>)->Data? {
        var dataResult : Data? = nil
        var request = NSMutableURLRequest(url: url)
        
        do {
            
            //convert params to json
            if let jsonData : Data = try JSONSerialization.data(withJSONObject: params, options:.prettyPrinted) as Data{
                let jsonString = String(data: jsonData as Data, encoding: .utf8)!
                print(jsonString)
                
                
                //preapre request
                request =  NSMutableURLRequest(url:url)
                request.setValue("application/json", forHTTPHeaderField: "Content-Type")
                request.setValue("application/json", forHTTPHeaderField: "Accept")
                
                //add method specific params
                if method == "post" {
                    request.httpMethod = "POST"
                    request.httpBody = jsonData as Data
                }
                else {
                    request.httpMethod = "GET"
                }
            }
            
            var response: URLResponse?
            let _ : NSError? = nil
            
            let data : Data = (try NSURLConnection.sendSynchronousRequest(request as URLRequest, returning: &response) as Data?)!
            dataResult = data
            
            if let httpResponse = response as? HTTPURLResponse {
                print(httpResponse.statusCode)
            }
        }
            
        catch let jsonErr {
            print("Error Serializion json:",jsonErr)
        }
        
        if let dataResultVal = dataResult {
            return dataResultVal
        }else {
            return nil
        }
        
    }
    
    //dönüş değerinde liste olan istekler için // array
    class func parseCollectionResult(data:Data)->GenericResponse {
        
        let localError : NSError? = nil
        let gr : GenericResponse = self.genericParse(data: data)
        
        do {
            
            let parsedObject = try JSONSerialization.jsonObject(with: data) as! [String:Any]
            
            print(parsedObject)
            
            if localError != nil {
                //return nil
            }
            
            let data =  parsedObject["products"]
            gr.dataArr = data as! [Dictionary<String, Any>]
        }
            // }
            //Handle Error
        catch let jsonErr {
            print("Error Serializin json:",jsonErr)
        }
        return gr
    }
    
    
    //dönüş değerinde tekil object olan istekler için // dictionary
    class func parseObjectResult(data:Data)->GenericResponse {
        
        let gr : GenericResponse = self.genericParse(data: data)
        
        do {
            
            let parsedObject =  try JSONSerialization.jsonObject(with: data) as! Dictionary<String,Any>
            
            print(parsedObject)
            
            if parsedObject != nil {
                
                gr.dataDict = (parsedObject as? Dictionary)!
                
            }
        }
        catch let jsonErr {
            print("Error Serializition json: ",jsonErr)
        }
        
        return gr
    }
    
    
    //dönüş nesnesindeki ortak veriler için
    class  func genericParse(data:Data)->GenericResponse {
        
        var gr : GenericResponse?
        let localError:NSError? = nil
        
        do {
            print(" ***** JSON WORK *********")
            
            if let parsedObject = try JSONSerialization.jsonObject(with: data) as? [String:Any] {
                
                if localError != nil {
                    print(localError!)
                }
                
                gr = GenericResponse(dataArr: [], dataDict: [:])
                
                print(gr!);
                
            }
        }
            //Handle Error
        catch let jsonErr {
            print("Error Serialization json:",jsonErr)
        }
        return gr!
    }
    
    
    func test_get_request_withURL() {
    
    
    }
    
}
