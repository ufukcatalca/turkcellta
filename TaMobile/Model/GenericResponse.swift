//
//  GenericResponse.swift
//
import UIKit
class GenericResponse: NSObject {
    
    var dataArr = [Dictionary<String,Any>]()
    var dataDict = Dictionary<String,Any>()
    
    //initialization to stored property
    init(dataArr:Array<Any>,dataDict:Dictionary<String, Any>) {
        
        //self.code = code
        //self.status = status
        //self.userMessage = userMessage
        //self.developerMessage = developerMessage
        self.dataArr = dataArr as! [Dictionary]
        self.dataDict = dataDict as! Dictionary
    }
    
}
