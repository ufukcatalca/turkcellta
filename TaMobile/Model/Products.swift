
import UIKit
class Products: NSObject {
    
    public var product_id : String? {
        didSet {
            print("Product_id  is set: \(product_id!)");
        }
    }
    
    public var name : String? {
        didSet {
            print("Name is set: \(name!)");
        }
    }
    
    public var price : Int? {
        
        didSet {
            print("Price is set: \(price!)");
        }
    }
    
    public var images :String? {
        didSet {
            print("Images is set: \(images!)");
            
        }
    }
    
    
    public var descriptions :String? //spesifik bir isim direk description şeklinde kullanılmaz
    {
        didSet {
            print("Descriptions is set: \(descriptions!)");
            
        }
    }
    
    public init(product_id:String, name:String,price:Int,images:String,descriptions :String) {
        
        super.init()
        
        ({self.product_id = product_id})();
        ({ self.name = name})();
        ({self.price = price})();
        ({self.images = images})();
        ({self.descriptions = descriptions})();
    }
}
