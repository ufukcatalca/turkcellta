// ########################################################################
// **************************** TAViewContoller ***************************
// ########################################################################

import UIKit
import NVActivityIndicatorView
import CoreData

class TAViewController: UIViewController, UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout,UISearchBarDelegate,UISearchResultsUpdating{
    
    var isDataLoading : Bool?
    var isPagingAvailable : Bool?
    var isRefreshing : Bool = false
    var isSearching = false
    
    var productsArr : [Products]?
    var product : Products?
    
    var selectedProductId:String?
    var selectedProductName:String?
    var selectedProductPrice:String?
    var selectedProductImage:String?
    var selectedProductDescription:String?
    
    //search text
    var userSearchText:String?
    var dataManager : DataManager?
    //refreshControl
    let refreshControl = UIRefreshControl()
    
    let moContext = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    
    //filteredArray
    var filteredDataArr = [Products]() //Array
    var activity:NVActivityIndicatorView?
    
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var productsCollectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        dataManager = DataManager.sharedInstance
        
        self.navigationItem.title = "TTA Mobile"
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "HelveticaNeue-Light", size: 19)!]
        self.navigationController?.navigationBar.barTintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = true;
        
        activity = NVActivityIndicatorView(frame:  CGRect(x: self.view.frame.width/2 - self.view.frame.width/8, y:self.view.frame.height/2 - self.view.frame.width/8, width: self.view.frame.width/4, height: self.view.frame.width/4), type: NVActivityIndicatorType.ballScaleMultiple, color: UIColor(red: (28/255), green: (146/255), blue: (210/255), alpha: 1.0), padding: NVActivityIndicatorView.DEFAULT_PADDING)
        
        self.view.addSubview(activity!)
        
        self.activity?.startAnimating()
        
        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here
            
            self.dataManager?.getAllProductList()
            
            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                
                self.loadProducts()
                
                // Init CollectionView and Register Collection View Cells
                
                self.activity?.stopAnimating()
                
                self.initCollectionViewAndRegisterCells()
                
                self.productsCollectionView.reloadData()
                
            }
        }
        
        self.navigationItem.backBarButtonItem = UIBarButtonItem.init(title: "", style: .plain, target: nil, action: nil)
        self.initCollectionViewAndRegisterCells()
    }
    
    
    override func viewWillDisappear(_ animated: Bool) {
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        //        var error:NSError?
        //        let request = NSFetchRequest<NSFetchRequestResult>(entityName:"product_id")
        //        product = moContext?.execute(request,error: &error) as [Products]
        //        self.productsCollectionView.reloadData()
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.loadProducts()
        self.setBackButtonAppearance();
    }
    
    @objc func refreshProductList(_ sender:Any){
        self.refreshControl.endRefreshing()
    }
    
    func setBackButtonAppearance() {
        
        self.navigationController?.view.backgroundColor = UIColor.white
        self.navigationController?.navigationBar.backIndicatorImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.backIndicatorTransitionMaskImage = UIImage(named: "back-black");
        self.navigationController?.navigationBar.tintColor = UIColor.black
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil);
    }
    
    
    @objc func loadProducts() {
        
        self.isDataLoading = true
        
        // Display a image when the CollectionView is empty
        
        let messageLabel = UILabel(frame:CGRect(x: 0, y:0 , width: self.view.bounds.size.width , height: self.view.bounds.size.height))
        messageLabel.sizeToFit()
        messageLabel.textColor = UIColor.gray
        messageLabel.numberOfLines = 2
        messageLabel.textAlignment = .center
        //messageLabel.text = "Products Loading.."
        messageLabel.font = UIFont.init(name: "Nunito-Light", size: 20)
        view.addSubview(messageLabel)
        
        self.productsCollectionView.backgroundView = messageLabel
        
        DispatchQueue.global(qos: .userInitiated).async {
            
            let _ = self.dataManager?.getAllProductList()
            
            DispatchQueue.main.async {
                
                self.isRefreshing = false
                
                self.productsArr = (self.dataManager?.productsArr)!
                
                self.filteredDataArr = self.dataManager!.productsArr
                
                self.productsDataToSave() // Core Data
                
                self.productsCollectionView.reloadData()
                
                self.isDataLoading = false
            }
        }
    }
    
    func initCollectionViewAndRegisterCells() {
        
        //set delegate and datasource
        self.productsCollectionView.delegate = self
        self.productsCollectionView.dataSource = self
        
        //register cells
        self.productsCollectionView.register(UINib(nibName: "TACollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "TACollectionViewCell")
        
        
        if #available(iOS 10, *) {
            
            self.productsCollectionView.refreshControl = self.refreshControl
            
        } else {
            
            self.productsCollectionView.addSubview(self.refreshControl)
            
        }
        
        self.refreshControl.addTarget(self, action: #selector(refreshCategories(_:)), for: .valueChanged)
        
        self.refreshControl.tintColor = UIColor.init(red: 103/255, green: 122/255, blue:152/255, alpha: 1.0);
        
        let multipleRefreshControlAttributes:[NSAttributedStringKey:Any] = [NSAttributedStringKey.font : UIFont(name: "HelveticaNeue", size: 15)!,
                                                                            NSAttributedStringKey.foregroundColor : UIColor.init(red: 103/255, green: 122/255, blue:152/255, alpha: 1.0)]
        
        self.refreshControl.attributedTitle = NSAttributedString(string: "TA güncelleniyor...", attributes: multipleRefreshControlAttributes)
        
    }
    
    @objc func refreshCategories(_ sender:Any) {
        
        DispatchQueue.global(qos: .background).async {
            // Background thread:
            // start loading your images here
            
            print("GUNCELLENDI");
            
            DispatchQueue.main.async {
                // Main thread, called after the previous code:
                // hide your progress bar here
                
                self.refreshControl.endRefreshing()
                self.productsCollectionView.reloadData()
                
            }
        }
    }
    
    
    // MARK: - SearchBardr DataSource and Properties
    
    func setSearchBarAppearence() {
        
        //Delegate
        self.searchBar.delegate = self;
        
        //searchbar layer
        searchBar.layer.borderWidth = 1
        searchBar.layer.borderColor = UIColor.white.cgColor
        
        searchBar.layer.shadowColor = UIColor.lightGray.cgColor
        searchBar.layer.shadowOffset = CGSize(width: 0, height: 1.0)
        searchBar.layer.shadowRadius = 10
        searchBar.layer.shadowOpacity = 0.6
        searchBar.layer.masksToBounds = false
        searchBar.layer.shadowPath = UIBezierPath(roundedRect: self.searchBar.bounds, cornerRadius: self.view.layer.cornerRadius).cgPath
        
        //Textfield in searchbar
        let textField = searchBar.value(forKey: "searchField") as! UITextField
        
        //searchbar left item set color
        let leftItem = textField.leftView as! UIImageView
        leftItem.image = leftItem.image?.withRenderingMode(UIImageRenderingMode.alwaysTemplate)
        leftItem.tintColor = UIColor(red: (28/255), green: (146/255), blue: (210/255), alpha: 1.0)
        
    }
    
    @objc func searchResignFirstResponder(sender: UISwipeGestureRecognizer) {
        self.searchBar.resignFirstResponder()
    }
    
    func searchBarTextDidBeginEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidBeginEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        isSearching = true
    }
    
    func searchBarTextDidEndEditing(_ searchBar: UISearchBar) {
        
        print("searchTextDidEndEditing: \(searchBar.text)");
        
        self.userSearchText = self.searchBar.text
        
        isSearching = false
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        isSearching = false
    }
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        
        print("searchBarTextDidChange: \(searchText)");
        self.userSearchText = searchText
        // If we haven't typed anything into the search bar then do not filter the results
        if searchBar.text! == "" {
            filteredDataArr = productsArr!
        } else {
            // Filter the results
            filteredDataArr = (productsArr?.filter { ($0.name?.lowercased().contains(searchBar.text!.lowercased()))! })!
        }
        
        self.productsCollectionView.reloadData()
    }
    
    func updateSearchResults(for searchController: UISearchController) {
        
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        
        print("SearchButtonClicked");
        
        self.searchBar.resignFirstResponder()
        
        isSearching = false
    }
    
    
    
    // MARK: - CollectionView DataSource
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        
        return filteredDataArr.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        let taCollectionViewCell = self.productsCollectionView.dequeueReusableCell(withReuseIdentifier: "TACollectionViewCell", for: indexPath) as? TACollectionViewCell
        
        self.product = self.filteredDataArr[indexPath.row] as? Products
        
        taCollectionViewCell?.productNameLabel.text = self.product?.name
        
        //Format Int to String
        let priceValue : String = String(format:"%d",(self.product?.price)!)
        taCollectionViewCell?.priceLabel.text = priceValue
        
        
        taCollectionViewCell?.categoryImage.image = UIImage(named:"default_category_icon") // yüklenirken gecici gösterilecek icon
        
        DispatchQueue.global(qos: .background).async {
            
            let iconUrlStr = self.product!.images!
            let imageUrl:URL = URL(string: iconUrlStr)!
            
            do {
                
                DispatchQueue.main.async {
                    // with Image Cash
                    taCollectionViewCell?.categoryImage.kf.setImage(with: imageUrl)
                }
            }
            catch {
                print("error");
            }
        }
        
        return taCollectionViewCell!
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        product = self.productsArr![(indexPath.row)] as? Products
        
        self.performSegue(withIdentifier: "productDetail", sender: self)
        
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        
        cell.layer.transform = CATransform3DMakeScale(0.1, 0.1, 1)
        
        UIView.animate(withDuration: 0.3, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05, 1.05, 1)
        },completion: { finished in
            UIView.animate(withDuration: 0.1, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.03, 1.03, 1)
            })
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        let cellWidth:CGFloat = (self.view.frame.size.width - 45.0) / 2
        let cellHeight:CGFloat = 240.0;
        
        return CGSize(width: cellWidth, height: cellHeight)
    }
    
    
    
    // MARK: - Navigation
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if segue.identifier == "productDetail" {
            let detailsVC: DetailTAViewController = segue.destination as! DetailTAViewController
            
            detailsVC.productToDetail = self.product
            
        }
    }
    
    // MARK: - Core Data
    
    func productsDataToSave() {
        
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        //let storeDescription = NSEntityDescription.insertNewObject(forEntityName: "product_id", into: moContext!)
        
        
        // let imageData: NSData = UIImagePNGRepresentation(self.Scannedimage.image!)! as NSData
        
        if #available(iOS 10.0, *) {
            //            if(self.saveImageDataToCoreData(imageData: imageData)){
            //
                            print("Data saved....")
            //            }
            //            else {
            //                print("Data didnt save....")
            //            }
            //        } else {
            //            // Fallback on earlier versions
        }
    }
    
    
    // *** To Saving Products CORE DATA ***
    @available(iOS 10.0, *)
    func saveImageDataToCoreData(imageData:NSData) -> Bool {
        
        let appDelegate = UIApplication.shared.delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext!
        let entity = NSEntityDescription.entity(forEntityName: "Images",
                                                in: managedContext)
        let options = NSManagedObject(entity: entity!,
                                      insertInto:managedContext)
        
        //        let newImageData = UIImageJPEGRepresentation(self.Scannedimage.image!,1)
        //
        //        options.setValue(newImageData, forKey: "image")
        //
        //        do {
        //            try managedContext.save()
        //        } catch
        //        {
        //            print("error")
        //        }
        
        return true
    }
    
    func read()
    {
        //let appDelegate = UIApplication.shared.delegate as! AppDelegate
        // let managedContext = appDelegate.managedObjectContext!
        // let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Data")
        
        var error: NSError?
        //        let fetchedResults = managedContext.executeFetchRequest(fetchRequest, error: &error)
        //            as [NSManagedObject]?
        
        //        if let results = fetchedResults
        //        {
        //            for (var i=0; i < results.count; i++)
        //            {
        //                let single_result = results[i]
        //                let out = single_result.valueForKey("name") as String
        //                print(out)
        //                //uncomment this line for adding the stored object to the core data array
        //                //name_list.append(single_result)
        //            }
        //        }
        //        else
        //        {
        //            print("cannot read")
        //        }
    }
    
    // Unit Test
    func getProductCount(num:Int) -> Bool {
        
        self.productsArr = (self.dataManager?.productsArr)!
        
        if productsArr!.count > 0 {
            return true
        }
        else {
            return false
        }

    }
}

