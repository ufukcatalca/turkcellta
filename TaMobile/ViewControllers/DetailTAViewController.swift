// ########################################################################
// **************************** DetailTAViewController ***************************
// ########################################################################

import UIKit
class DetailTAViewController: UIViewController {

    let dataManager = DataManager.sharedInstance
    //Products
    var productToDetail:Products?
    
    //for Detail
    public var productId:String?
    public var productName:String?
    public var productPrice:String?
    public var productDescription:String?
    public var productImage:String?

    
    @IBOutlet weak var productIdLabel: UILabel!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var productImageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = productToDetail?.name
        
        self.dataManager.selectedProduct_id["product_id"] = productToDetail?.product_id
        
        self.productDescription =  self.dataManager.getProductDetail(product_id:(productToDetail?.product_id)!)
        
        self.productToDetail?.descriptions = self.productDescription
        
        let urlStr = productToDetail?.images
        let imageUrl:URL = URL(string: urlStr!)!

        // Start background thread so that image loading does not make app unresponsive
        DispatchQueue.global(qos: .userInitiated).async {

            let imageData:NSData?

            imageData = NSData(contentsOf: imageUrl)

            if(imageData == nil){
                //image yok
                DispatchQueue.main.async {
                    let imageName = "default_category_icon"
                    self.productImageView.image = UIImage(named: imageName)
                }
            }else {
                // When from background thread, UI needs to be updated on main_queue
                DispatchQueue.main.async {
                    let image = UIImage(data: imageData! as Data)
                    self.productImageView.image = image
                    self.productImageView.layer.cornerRadius = self.productImageView.frame.size.width/2
                    self.productImageView.layer.masksToBounds = true
                    self.productImageView.clipsToBounds = true
                    self.productImageView.layer.shadowColor = UIColor.lightGray.cgColor
                    self.productImageView.layer.shadowOffset = CGSize(width: 0, height: 1.0)
                    self.productImageView.layer.shadowRadius = 10
                    self.productImageView.layer.shadowOpacity = 0.6
                    self.productImageView.layer.masksToBounds = false
                    self.productImageView.layer.shadowPath = UIBezierPath(roundedRect: self.productImageView.bounds, cornerRadius: self.view.layer.cornerRadius).cgPath

                    self.view.addSubview(self.productImageView)
                    
                    
                }
            }
        }
    
        productIdLabel.text = productToDetail?.product_id
        nameLabel.text = productToDetail?.name
        let priceValue : String = String(format:"%d",(self.productToDetail?.price)!)
        priceLabel.text = priceValue
        descriptionLabel.text = productToDetail?.descriptions
    
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

}
