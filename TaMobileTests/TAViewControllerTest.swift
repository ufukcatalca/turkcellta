//
//  TAViewControllerTest.swift
//  TaMobileTests
//

import XCTest
@testable import TaMobile

class TAViewControllerTest: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func getProductCount(){
        let viewController = TAViewController()
        
        let countProducts = 10

        XCTAssertTrue(viewController.getProductCount(num:countProducts))
        XCTAssertFalse(viewController.getProductCount(num:countProducts))
        
    }

    
}
