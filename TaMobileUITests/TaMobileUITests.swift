//
//  TaMobileUITests.swift
//  TaMobileUITests
//
//  Created by Ufuk Catalca on 12/27/17.
//  Copyright © 2017 Turkcell. All rights reserved.
//

import XCTest

class TaMobileUITests: XCTestCase {
        
    override func setUp() {
        super.setUp()
        
        // Put setup code here. This method is called before the invocation of each test method in the class.
        
        // In UI tests it is usually best to stop immediately when a failure occurs.
        continueAfterFailure = false
        // UI tests must launch the application that they test. Doing this in setup will make sure it happens for each test method.
        XCUIApplication().launch()

        // In UI tests it’s important to set the initial state - such as interface orientation - required for your tests before they run. The setUp method is a good place to do this.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() { // UI Testing
        
        let tablesQuery = XCUIApplication().tables
        tablesQuery/*@START_MENU_TOKEN@*/.collectionViews/*[[".cells.collectionViews",".collectionViews"],[[[-1,1],[-1,0]]],[0]]@END_MENU_TOKEN@*/.children(matching: .cell).element(boundBy: 1).children(matching: .other).element.tap()
        
        let appNameIsSoBeautifulCellsQuery = tablesQuery.cells.containing(.staticText, identifier:"App Name is so beautiful")
        appNameIsSoBeautifulCellsQuery.children(matching: .staticText).matching(identifier: "App Name is so beautiful").element(boundBy: 1).tap()
        appNameIsSoBeautifulCellsQuery.children(matching: .staticText).matching(identifier: "App Name is so beautiful").element(boundBy: 0).tap()
        
        // Use recording to get started writing UI tests.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testTaViewController() {
        
        let app = XCUIApplication()
        app.tabBars.buttons["Home"].tap()
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 2).otherElements.containing(.staticText, identifier:"167").element.swipeUp()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 3).otherElements.containing(.staticText, identifier:"543").element.swipeUp()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 5).otherElements.containing(.staticText, identifier:"272").element.swipeUp()
        app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .collectionView).element.swipeDown()
        
        let cellsQuery = collectionViewsQuery.cells
        cellsQuery.otherElements.containing(.staticText, identifier:"267").element.swipeDown()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 7).otherElements.containing(.staticText, identifier:"9").element.swipeDown()
        cellsQuery.otherElements.containing(.staticText, identifier:"9").element.swipeDown()
        collectionViewsQuery.children(matching: .cell).element(boundBy: 1).staticTexts["120"].swipeDown()
        
    }
    
    func testTaViewControllerSearchBar() {
        
        let app = XCUIApplication()
        app.tabBars.buttons["Home"].tap()
        
        let searchProductSearchField = app.searchFields["Search Product..."]
        searchProductSearchField.tap()
        searchProductSearchField.typeText("Onions")
        
        let collectionViewsQuery = app.collectionViews
        collectionViewsQuery.children(matching: .cell).element(boundBy: 2).otherElements.containing(.staticText, identifier:"110").element.swipeUp()
        
        let element = collectionViewsQuery.children(matching: .cell).element(boundBy: 0).otherElements.containing(.staticText, identifier:"110").element
        element.swipeDown()
        element.tap()
        
        let element2 = app.children(matching: .window).element(boundBy: 0).children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element.children(matching: .other).element
        element2.swipeUp()
        element2.swipeDown()
        
        let applesStaticText = app.staticTexts["Apples"]
        applesStaticText.swipeUp()
        element2.swipeDown()
        
        let anAppleADayKeepsTheDoctorAwayStaticText = app.staticTexts["An apple a day keeps the doctor away."]
        anAppleADayKeepsTheDoctorAwayStaticText.tap()
        anAppleADayKeepsTheDoctorAwayStaticText.tap()
        anAppleADayKeepsTheDoctorAwayStaticText.tap()
        element2.tap()
        applesStaticText.tap()
        app.staticTexts["1"].tap()
        app.staticTexts["120"].tap()
        anAppleADayKeepsTheDoctorAwayStaticText.tap()
        
    }
    
    func tryToUseApp(){
        
        let app = XCUIApplication()
        app.tabBars.buttons["Home"].tap()
        let searchProductSearchField = app.searchFields["Search Product..."]
        searchProductSearchField.tap()
        searchProductSearchField.typeText("Onions")
    
    }
    

}
